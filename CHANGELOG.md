# Changelog


## July 21, 2024 (version 1.7.1)
### Continuous integration
- **ci:** fix token (e5ae6283)
- **ci:** migrate to new poetry package template ci (4b298a2b)

The update addresses the following issues:
- [Migrate All pipelines to new poetry CI template](https://gitlab.com/romanlabs/ci-templates/package-publish/-/issues/1)

Migrated pipeline to new release format