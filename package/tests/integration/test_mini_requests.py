#
# Copyright (C) 2024 RomanLabs, Rafael Roman Otero
# This file is part of RLabs Mini API.
#
# RLabs Mini API is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RLabs Mini API is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with RLabs Mini API. If not, see <http://www.gnu.org/licenses/>.
#
'''
    Test rlabs_mini_api
'''
import os
import logging
from time import sleep

from rlabs_mini_api.request import GET
from rlabs_mini_api.request import POST
from rlabs_mini_api.request import DELETE
from rlabs_mini_api.request import PUT
from rlabs_mini_api.request import Request

GITLAB_API_URL = "https://gitlab.com/api/v4"
DUMMY_TEST_GROUP_ID = 79866152
TOKEN = os.environ['TOKEN']

def test_rlabs_mini_requests():
    '''
        Test rlabs_mini_requests.
    '''
    Request.config(
        GITLAB_API_URL,
        { "PRIVATE-TOKEN":TOKEN },
        retries=3,
        retry_base_delay=2.0,
        general_timeout=10.0,
        log_level=logging.INFO
    )

    #  -- Delete variable --
    DELETE                          \
        .groups                     \
        .id(DUMMY_TEST_GROUP_ID)    \
        .variables                  \
        .key("dummy_var")           \
        .exec()

    sleep(1)
    response = GET                          \
        .groups                             \
        .id(DUMMY_TEST_GROUP_ID)            \
        .variables(page=1, per_page=100)    \
        .exec()

    vars = [
        { var['key'] :  var['value'] }
        for var in response.python_data
    ]

    assert { 'dummy_var' : 'dummy_val' } not in vars

    #  -- Create variable --
    POST                            \
        .groups                     \
        .id(DUMMY_TEST_GROUP_ID)    \
        .variables(
            key="dummy_var",
            value="dummy_val"
        )                           \
        .exec()

    sleep(1)
    response = GET                          \
        .groups                             \
        .id(DUMMY_TEST_GROUP_ID)            \
        .variables(page=1, per_page=100)    \
        .exec()

    vars = [
        { var['key'] :  var['value'] }
        for var in response.python_data
    ]

    assert { 'dummy_var' : 'dummy_val' } in vars

    #  -- Update variable --
    PUT(data={
        "value": "UPDATED_VALUE"
    })                              \
        .groups                     \
        .id(DUMMY_TEST_GROUP_ID)    \
        .variables                  \
        .key("dummy_var")           \
        .exec()

    sleep(1)
    response = GET                          \
        .groups                             \
        .id(DUMMY_TEST_GROUP_ID)            \
        .variables(page=1, per_page=100)    \
        .exec()

    vars = [
        { var['key'] :  var['value'] }
        for var in response.python_data
    ]

    assert { 'dummy_var' : 'UPDATED_VALUE' } in vars

def test_name_private_name_collision():
    '''
        Test Name collisions.
    '''
    # These are now private and users
    # can use them for their URLs
    GET                 \
        .data           \
        .content        \
        .params         \
        .http_method    \
        .path
