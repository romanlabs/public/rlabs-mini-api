<div style="text-align: center;">
<img src="images/logo.png" alt="Demo" width="300">
</div>

# Table of Contents
- [RLabs Mini API](#rlabs-mini-api)
- [Installation](#installation)
- [Usage](#usage)
  - [Import](#import)
  - [Configure](#configure)
  - [Method Chaining notation](#method-chaining-notation)
  - [Example - Create a Gitlab Variable](#example---create-a-gitlab-variable)
  - [Example - Updating a Gitlab Variable](#example---updating-a-gitlab-variable)
  - [Example - Deleting a Gitlab Variable](#example---deleting-a-gitlab-variable)
  - [Response and Databox Operation Logging](#response-and-databox-operation-logging)
- [Complete Example](#complete-example)
- [License](#license)
- [Copyright](#copyright)


# RLabs Mini API

RLabs Mini API is a simple Python library for making API requests. Intended for quick prototyping.

# Installation

```bash
pip3 install rlabs-mini-api
```

# Usage

## Import

```python
from rlabs_mini_api.request import Request
```

import different HTTP methods:

```python
from rlabs_mini_api.request import GET
from rlabs_mini_api.request import POST
from rlabs_mini_api.request import DELETE
from rlabs_mini_api.request import PUT
```

## Configure

```python
 Request.config(
    GITLAB_API_URL,
    { "PRIVATE-TOKEN":TOKEN },
    retries=3,
    retry_base_delay=2.0,
    general_timeout=17.0,
    log_level=logging.INFO,
)
```

`Request.config`'s signature is as follows:

```python
@staticmethod
def config(
    base_url: str,
    headers: Dict[str, str],
    retries: int = 3,
    retry_base_delay: float = 0.5,
    general_timeout: Optional[float] = 7.0,
    log_level: Optional[int] = None,
    logger_override: Optional[logging.Logger] = None,
    response_log_dir: Optional[Path] = None
) -> None:
```

- `base_url`: The base URL that will be preppended to all URLs when forming requests, e.g. `https://gitlab.com/api/v4` 
- `headers`: HTTP headers to use. `RLabs Mini API` uses [httpx](https://www.python-httpx.org/), so this parameter is simply passed along.
- `retries`: number of retries to make when making the request
- `retry_base_delay`: base delay for retries to use when calculating retry delay:
```python
# Sleep for an exponentially increasing delay
delay = Request.retry_base_delay*(2.0**attempt-1)
```
- `general_timeout`: Common value to use for read, write, and connection timeouts, passed along to [httpx](https://www.python-httpx.org/).
- `log_level`: configures the log level of the built-in logger
- `logger_override`: overrides the custom built-in logger
- `response_log_dir`:  directory where all API responses are logged to when the logger's log level is `DEBUG`.

## Method Chaining notation

The library lets you specify API endpoints using method chaining notation. So for example, this is the Gitlab endpoint to read variables (`:id` stands for the path variable `id`):

```
GET /groups/:id/variables
```

if we were to read the first 100 variables for group with group id `11111` the request endpoint would be:

```
GET https://gitlab.com/api/v4/groups/11111/variables?page=1&&per_page=100
```

which in `RLabs Mini API` is expressed as:

```python
1 response = GET                          \
2     .groups                             \
3     .id(DUMMY_TEST_GROUP_ID)            \
4     .variables(page=1, per_page=100)
```

The way the URL to be requested is constructed is as follows:

- (1) begin with the method specified: `GET`
- (1) use `Requesy.base_url` as specified in `Request.config`: `GET https://gitlab.com/api/v4`
- (2) add the aubpath `groups` to the path: `GET https://gitlab.com/api/v4/groups`
- (3) add the path variable `DUMMY_TEST_GROUP_ID` to the path: `GET https://gitlab.com/api/v4/groups/DUMMY_TEST_GROUP_ID`
    - when passing an **argument** to a method in the chain, it is interpreted as "this is a path variable", so the method name is not included in the path, only the parameter passed
- (4) add the subpath `variables` to the path: `GET https://gitlab.com/api/v4/groups/DUMMY_TEST_GROUP_ID/variables`
- (4) add the query parameters `page1` and `per_page=100`: `GET https://gitlab.com/api/v4/groups/DUMMY_TEST_GROUP_ID/variables?page=1&per_page=100`
    - when passing a **keyword argument** to a method in the chain, it is interpreted as "this is a query param"

That constructs the request, then all is left is to execute it by calling `.exec()` at the end:

```python
response = GET                          \
    .groups                             \
    .id(DUMMY_TEST_GROUP_ID)            \
    .variables(page=1, per_page=100)    \
    .exec()
```

`.exec()` will return a `Response`  object specifying a few things:

```python
@dataclass
class Response:
    attempts_made: int
    status_code: int
    headers: Dict[str, str]
    text: str
    python_data: Optional[Any]
    json_data: Optional[str]
    databox: Optional[Box]
```

some of the values above are passed along from the [httpx library](https://www.python-httpx.org/), such as status code, text, and headers:

```python
python_data = response.json() if response.headers.get('content-type') == 'application/json' else None
json_data = json.dumps(python_data, indent=2) if python_data else None

return Response(
    attempts_made=attempt,
    status_code=response.status_code,   
    headers=dict(response.headers),
    text=response.text,
    python_data=python_data,
    json_data=json_data
    databox=Box(python_data)
)
```

- `attempts_made`: how many attempts took to execute the request
- `status_code`: status code included in the response (from httpx)
- `headers`: headers included in the response (from httpx)
- `text`: response's text (from httpx)
- `python_data`: the response's text (assuming it's valid JSON) parsed to a python object
- `json_data`: `python_data` as JSON
- `databox`. an object from the package [RLabs MiniBox](https://gitlab.com/romanlabs/public/rlabs-mini-box) which simplifies creating simple data pipelines. 

## Example - Create a Gitlab Variable

```python
#
# POST /groups/DUMMY_TEST_GROUP_ID/variables?key=dummy_var&value=dummy_val
#
POST                            \
    .groups                     \
    .id(DUMMY_TEST_GROUP_ID)    \
    .variables(
        key="dummy_var",
        value="dummy_val"
    )                           \
    .exec()

#
# GET /groups/DUMMY_TEST_GROUP_ID/variables?page=1&per_page=100
#
response = (GET
    .groups
    .id(DUMMY_TEST_GROUP_ID)
    .variables(page=1, per_page=100)
    .exec()
)

variables = (response.databox
    .map(
        lambda x: f"{x["key"]}={x["value"]}"
    )
    .to_json(
        indent=2
    )
    .data()
)

print(
    variables
)
```
Output:

![image](images/creating_variable.png)

## Example - Updating a Gitlab Variable

```python
#
# PUT /groups/DUMMY_TEST_GROUP_ID/variables/dummy_var
#
PUT(data={
    "value": "UPDATED_VALUE"
})                              \
    .groups                     \
    .id(DUMMY_TEST_GROUP_ID)    \
    .variables                  \
    .key("dummy_var")           \
    .exec()

#
# GET /groups/DUMMY_TEST_GROUP_ID/variables?page=1&per_page=100
#
response = (GET
    .groups
    .id(DUMMY_TEST_GROUP_ID)
    .variables(page=1, per_page=100)
    .exec()
)

variables = (response.databox
    .map(
        lambda x: f"{x["key"]}={x["value"]}"
    )
    .to_json(
        indent=2
    )
    .data()
)

print(
    variables
)
```

Output:

![image](images/updating_variable.png)

## Example - Deleting a Gitlab Variable

```python
try:
    #
    # DELETE /groups/:id/variables/:key
    #
    DELETE                          \
        .groups                     \
        .id(DUMMY_TEST_GROUP_ID)    \
        .variables                  \
        .key("dummy_var")           \
        .exec()
except Exception as e:
    if "404" in str(e):
        pass

#
# GET /groups/DUMMY_TEST_GROUP_ID/variables?page=1&per_page=100
#
response = (GET
    .groups
    .id(DUMMY_TEST_GROUP_ID)
    .variables(page=1, per_page=100)
    .exec()
)

variables = (response.databox
    .map(
        lambda x: f"{x["key"]}={x["value"]}"
    )
    .to_json(
        indent=2
    )
    .data()
)

print(
    variables
)
```

Output:

![image](images/deleting_variable.png)


## Response and Databox Operation Logging

When `log_level` is set to `DEBUG` every response is logged to the directory `response_log_dir`/response, and every data transformation on `response.databox` is logged to the `response_log_dir`/operations:

![Demo](images/operations_log_dir.png)

# Complete Example

```python
import os
import logging
from pathlib import Path

from rlabs_mini_api.request import GET
from rlabs_mini_api.request import POST
from rlabs_mini_api.request import DELETE
from rlabs_mini_api.request import PUT
from rlabs_mini_api.request import Request


GITLAB_API_URL = "https://gitlab.com/api/v4"
DUMMY_TEST_GROUP_ID = 79866152

def main():
    '''
        main
    '''

    ##
    ##  There's no main in a package, this is just a sample
    ##  for when building the classes
    ##
    ##  Replace this with a test
    ##

    # Configure for all requests
    Request.config(
        GITLAB_API_URL,
        { "PRIVATE-TOKEN": os.environ['TOKEN'] },
        retries=3,
        retry_base_delay=2.0,
        general_timeout=17.0,
        log_level=logging.DEBUG,
        response_log_dir=Path("../logs")
    )

    ## -- Delete variable --
    try:
        #
        # DELETE /groups/:id/variables/:key
        #
        DELETE                          \
            .groups                     \
            .id(DUMMY_TEST_GROUP_ID)    \
            .variables                  \
            .key("dummy_var")           \
            .exec()
    except Exception as e:
        if "404" in str(e):
            pass

    #
    # GET /groups/:id/variables?page=1&per_page=100
    #
    response = (GET
        .groups
        .id(DUMMY_TEST_GROUP_ID)
        .variables(page=1, per_page=100)
        .exec()
    )

    variables = (response.databox
        .map(
            lambda x: f"{x["key"]}={x["value"]}"
        )
        .to_json(
            indent=2
        )
        .data()
    )

    print(
        variables
    )


    # ## -- Create variable --
    #
    # POST /groups/:id/variables?key=dummy_var&value=dummy_val
    #
    POST                            \
        .groups                     \
        .id(DUMMY_TEST_GROUP_ID)    \
        .variables(
            key="dummy_var",
            value="dummy_val"
        )                           \
        .exec()

    #
    # GET /groups/:id/variables?page=1&per_page=100
    #
    response = (GET
        .groups
        .id(DUMMY_TEST_GROUP_ID)
        .variables(page=1, per_page=100)
        .exec()
    )

    variables = (response.databox
        .map(
            lambda x: f"{x["key"]}={x["value"]}"
        )
        .to_json(
            indent=2
        )
        .data()
    )

    print(
        variables
    )

    # ## -- Update variable --
    #
    # PUT /groups/:id/variables/:key
    #
    PUT(data={
        "value": "UPDATED_VALUE"
    })                              \
        .groups                     \
        .id(DUMMY_TEST_GROUP_ID)    \
        .variables                  \
        .key("dummy_var")           \
        .exec()

    #
    # GET /groups/:id/variables?page=1&per_page=100
    #
    response = (GET
        .groups
        .id(DUMMY_TEST_GROUP_ID)
        .variables(page=1, per_page=100)
        .exec()
    )

    variables = (response.databox
        .map(
            lambda x: f"{x["key"]}={x["value"]}"
        )
        .to_json(
            indent=2
        )
        .data()
    )

    print(
        variables
    )

if __name__ == "__main__":
    main()
```

output:

![Demo](images/complete_example.png)

# License

This project is licensed under the GNU Lesser General Public License v3.0 - see the [LICENSE](./LICENSE) file for details.

# Copyright

Copyright (C) 2024 RomanLabs, Rafael Roman Otero